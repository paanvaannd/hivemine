#!/usr/bin/env python
# -*- coding: utf-8 -*-

# WARN: Module-level docstring also "prog_desc" of "cli.py"
# WARN: If one is updated, the update should be mirrored accordingly.
"""Finds words in a file that include specified letters.

Functions
---------
main()
    Execute search for query criteria specified in configuration file.

Copyright (C) 2022 Pavan Anand

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program under the filename "LICENSE".  If not, see
<https://www.gnu.org/licenses/>.
"""

# Import built-in libraries
import os
import time

# Import custom modules
import cli
import cfg
import engine
import path


def main() -> None:
    """Execute search for query criteria specified in configuration file."""
    opts = cli.load_args()  # Consider duck typing
    config: dict = cfg.load_config(opts.config)
    if opts.v: print("Configuration loaded.")

    hard, soft = engine.extract_queries(config)
    exclude = engine.derive_exclusions(hard + soft)
    if opts.v: print(f"Query terms loaded from {opts.config}.")

    if opts.v:
        print("Starting search...")
        if opts.v >= 2: t = time.perf_counter()
    solutions = engine.search(opts.source, ((True, hard, False),
                                            (False, soft, False),
                                            (False, exclude, True)))
    if opts.v >= 2: print(f"Took {time.perf_counter() - t} sec. to match queries.")

    filepath = path.get_full_path(opts.output)
    save_location = path.get_ancestor_path(filepath, 1)
    if not os.path.exists(save_location):
        path.create_directory(save_location)
    if opts.v:
        print(f"Saving results to {filepath}...")
        if opts.v >= 2: t = time.perf_counter()
    # ISSUE: File with same name will be overwritten
    with open(filepath, "w+") as file:
        file.writelines(solutions)
    if opts.v >= 2: print(f"Took {time.perf_counter() - t} sec. to save file.")

    if opts.v >= 2: print("Finished.")

if __name__ == "__main__":
    main()
