#/usr/bin/env python
# -*- coding: utf-8 -*-

"""Loads configuration information passed to program in command line.

Functions
---------
load_config(filename: str) -> dict
    Deserializes JSON data in specified file.
"""

# Import built-in libraries
import json
import os
import sys

# Import custom modules
import cli

# TODO: Download default config file from GitLab Snippet if one is not
# found
# If working offline or URL not available, throw error explaining
# that an example can be found in the README.
# TODO: Change config file type from JSON to something more readable
# E.g., TOML, YAML, INI
def load_config(filename: str) -> dict:
    """Deserialize JSON data in specified file.

    Parameters
    ----------
    filename : str
        Name of file from which data will be loaded.

    Returns
    -------
    dict
        Deserialized contents of configuration file.
    """

    file_opened: bool = False
    while not file_opened:
        if os.path.isfile(filename):
            with open(filename, "r") as file:
                _config: dict  = json.load(file)
                return _config
        else:
            msg: str = ("Error: config file not found. Please retry after creating "
                        "one.")
            sys.exit(msg)
