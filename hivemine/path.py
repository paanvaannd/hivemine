#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""...

Functions
---------
get_ancestor_path(path: str, level: int)
    Find the nth parent directory of a path, where n = \"level\".
create_directory(path: str)
    Create directory at specified path.
disambiguate_relative_path(relative_path: str)
    Expand relative path symbols to text.
get_full_path(path_fragment: str)
    Generate a path from filesystem root to the specified path.
"""

# Import built-in libraries
import os
import sys


def get_ancestor_path(path: str, level: int) -> str:
    """Find the nth parent directory of a path, where n = \"level\".

    Parameters
    ----------
    path: str
        Path for whose nth-level parent should be found.
    level: int
        Number of \"path\"'s parent directories to traverse.

    Returns
    -------
    str
        The nth parent directory of \"path\", where n = \"level\".
    """
    breadcrumb: list = path.split("/")
    return "/".join(breadcrumb[:-level])


# ISSUE: Program not scriptable due to required input
# TODO: Add prompt auto-acception/-rejection
def create_directory(path: str) -> None:
    """Create directory at specified path.

    Parameters
    ----------
    path: str
        Path at which directory should be created.
    """
    decision: str = input("The path you specified does not exist. "
                          "Would you like the following path to be created?\n"
                          f"{path}\n"
                          "[Y/n] ")
    decision = decision.strip().lower()
    accepted_responses = {"y", "yes"}
    consent_attained: bool = True if decision in accepted_responses else False
    if consent_attained:
        os.mkdir(path)
    else:
        print("Unable to create directory; please try again.")
        sys.exit()


def disambiguate_relative_path(relative_path: str) -> str:
    """Expand relative path symbols to text.

    Parameters
    ----------
    relative_path: str
        Path containing a symbol to expand.

    Returns
    -------
    expanded_fragment
        Expaneded, textual representation of input relative path.
    """
    first_character, second_character = relative_path[0:2]
    if first_character == "." and second_character == "/":
        expanded_fragment = f"{os.getcwd()}/{relative_path[2:]}"
    elif first_character == "." and second_character == ".":
        expanded_fragment = f"{os.path.dirname(os.getcwd())}/{relative_path[3:]}"
    elif first_character == "~":
        expanded_fragment = f"{os.path.expanduser('~')}/{relative_path[2:]}"
    elif first_character == "/":
        expanded_fragment = f"{os.getcwd()}/{relative_path[1:]}"
    else:
        expanded_fragment = f"{os.getcwd()}/{relative_path}"
    return expanded_fragment


# NOTE: More general variable names & documentation can be applied here as this
# is now better uncoupled from the initial function of simply saving a file.
# TODO: Automatically create path if target within script root
def get_full_path(path_fragment: str) -> str:
    """Generate a path from filesystem root to the specified path.

    Parameters
    ----------
    path_fragment: str
        Path of file in which data will be saved. If only a filename is
        provided, the results file will be saved to the default directory
        (<project_root>/results).

    Returns
    -------
    str
        Full path (i.e., path starting at filesystem root) to path specified
        in \"path_fragment\".
    """
    if "/" in path_fragment:
        expanded_fragment = disambiguate_relative_path(path_fragment)
    else:
        project_path: str = get_ancestor_path(os.path.realpath(__file__), 2)
        default_directory: str = f"{project_path}/results"
        expanded_fragment: str = f"{default_directory}/{path_fragment}"
    path_parent: str = get_ancestor_path(expanded_fragment, 1)
    filename = os.path.basename(expanded_fragment)
    return f"{path_parent}/{filename}"
