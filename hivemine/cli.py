#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Handles accepting arguments from user via the command line.

Functions
---------
load_args()
    Return ArgumentParser object with specified parameters.
"""

# Import built-in libraries
import argparse
from datetime import datetime

# TODO: Define return type... duck type?
def load_args():
    """Return ArgumentParser object with specified parameters."""
    return parser.parse_args()


prog_name = "hivemine"
# WARN: Module-level docstring also "prog_desc" of "cli.py"
# WARN: If one is updated, the update should be mirrored accordingly.
prog_desc = "Finds words in a file that include specified letters."
parser = argparse.ArgumentParser(prog = prog_name,
                                 description = prog_desc,
                                 allow_abbrev = False)
parser.version = "0.3.0"

config_help = ("JSON file specifying characters to search for and minimum "
               "and/or maximum word length (optionally)")
parser.add_argument("-c",
                    "--config",
                    type = str,
                    metavar = "CONFIG",
                    nargs = "?",
                    default = "config.json",
                    action = "store",
                    help = config_help) 

# TODO: Allow user to specify a whole directory or mulitple files
source_help = ("plaintext, UTF-8-encoded file (containing 1 word per line) to "
               "be searched; defaults to \"$(pwd)/source.txt\"")
parser.add_argument("-s",
                    "--source",
                    type = str,
                    metavar = "SOURCE",
                    nargs = "?",
                    default = "source.txt",
                    action = "store",
                    help = source_help)

current_timestamp = datetime.now()
datetime_format = "%Y-%m-%d-%H%M%S"
output_help = "name of file where matched lines should be saved"
parser.add_argument("-o",
                    "--output",
                    type = str,
                    metavar = "OUTPUT",
                    nargs = "?",
                    default = f"results-{current_timestamp.strftime(datetime_format)}.txt",
                    action = "store",
                    help = output_help)

version_help = "display current hivemine version"
parser.add_argument("--version",
                    action = "version",
                    help = version_help)

verbosity_help = ("specify output verbosity level; repeat flag to increase "
                  "level (max level = 3)")
parser.add_argument("-v",
                    action = "count",
                    default = 0,
                    help = verbosity_help)
