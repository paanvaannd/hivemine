#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Handles parsing of text.

Functions
---------
derive_exclusions(queries: tuple) -> tuple
    Remove characters constituting query from valid characters.
derive_queries(config: dict) -> tuple
    Extract query characters from configuration file.
match(greedy: bool, query: str, neg: bool, test: str) -> bool
    Check whether a word is matched by a query.
search(filename: str, expr) -> tuple
    Run query against words within a file.
"""

# Import built-in libraries
import string
import sys


def derive_exclusions(queries: tuple) -> tuple:
    """Remove characters constituting query from valid characters.

    Parameters
    ----------
    queries : tuple
        Characters constituting query to match.

    Returns
    -------
    tuple
        All alphanumeric characters minus query characters.
    """

    exclude: list = [str(c) for s in (string.ascii_letters, range(10)) for c in s]
    for letter in queries: exclude.remove(letter)
    return tuple(exclude)


# TODO: Generalize this by removing coupling with config file or relocating
# to live withing cfg.py if it remains tightly coupled to the config file
def extract_queries(config: dict) -> tuple:
    """Extract query characters from configuration file.

    Parameters
    ----------
    config : dict
        Deserialized contents of configuration file.

    Returns
    -------
    tuple
        Tuple of sets, each containing query characters.
    """

    try:
        hard: tuple = tuple(set(config["letters"][0]["hard"].lower().strip()))
        soft: tuple = tuple(set(config["letters"][1]["soft"].lower().strip()))
        return (hard, soft)
    except AttributeError:
        msg: str = ("Error: ensure that the values of 'hard' and 'soft' in "
                    "config.json are strings.")
        sys.exit(msg)


def match(greedy: bool, query: str, neg: bool, test: str) -> bool:
    """Check whether a word is matched by a query.

    Parameters
    ----------
    query : str
        Characters constituting query to match.

    Returns
    -------
    bool
        True if test string is matched by query, otherwise False.
    """

    criterion = all if greedy else any
    result: bool = criterion(c in test for c in query)
    return result if not neg else not result


def search(filename: str, expr: tuple) -> tuple:
    """Run query against words within a file.

    Parameters
    ----------
    filename : str
        Name of file containing data to run queries against.
    expr : tuple
        Query data, including—in order—1) whether all (True, "greedy")
        or any (False, "non-greedy") of the query characters should be
        matched; 2) the query characters; & 3) whether or not the query
        should or should not be matched in the tested string.

    Returns
    -------
    tuple
        All strings that were matched by the query.
    """

    solutions: list = []
    with open(filename, "r") as file:
        wordlist: set = set(file.read().splitlines())
        for word in wordlist:
            if all(match(*expr[i], word) for i in range(len(expr))):
                solutions.append(word + "\n")
    return tuple(sorted(solutions))
