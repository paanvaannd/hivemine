# Hivemine

Copyright (C) 2022 Pavan Anand

## Background

Hivemine was born out of a simple question: what the devil is that one last word keeping me from reaching Queen Bee status on the New York Times Spelling Bee game?

If you're unfamiliar with the game, here is a simple overview: you are presented with 7 English letters and are asked to make words out of only those letters. The words must be longer than 4 letters in length and must contain at least one instance of the center letter (the letters are presented in a honeycomb shape, with 6 optional letters circling the necessary letter).

The New York Times uses a private list of words to populate the list of acceptable answers. Not all English words are accepted, and only American English spellings of words are accepted if there is any international variation in spelling.

While researching potential openly accessible word lists to mine for solutions, I discovered the SCOWL (Spell Checker Oriented Word Lists) and Friends database, which offered an open source aggregate list separated by word rarity between different English variations (e.g., American English vs. British English) as a Git repository: perfect for the job! So I got to cloning and hacking away, resulting in the program presented here.

### Origin of the name

The New York Times affectionately calls players of their Spelling Bee game the "hivemind," a pun on how the player base, like honeybees, will swarm together online or in-person to complete a task. Since this program facilitates the job of the "hive"-mind through "mine"-ing data from a list of words, I settled on the meta-pun of "Hivemine" as its title.

## Installation

*TODO*: This project is a work in progress. Please excuse the lack of clear documentation for program installation and usage for the time being. Rest assured that it will be added soon!

## Usage

*TODO*: This project is a work in progress. Please excuse the lack of clear documentation for program installation and usage for the time being. Rest assured that it will be added soon!
